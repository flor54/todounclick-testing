from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import Profile,FormaPago

# class RequiredFormSet(forms.models.BaseInlineFormSet):
#       def __init__(self, *args, **kwargs):
#           super(RequiredFormSet, self).__init__(*args, **kwargs)
#           self.forms[0].empty_permitted = False
class FormaPagoInline(admin.StackedInline):
    model = FormaPago
    can_delete = False
    verbose_name_plural = 'FormaPago'
    fk_name = 'user'

class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'
    # def clean(self):
    # 	user_ref = self.cleaned_data.get('usuario_referencia')
    # 	if not User.objects.filter(username=user_ref).exists():
    # 		raise forms.ValidationError("user ref not valid")
    # 	return self.cleaned_data

class CustomUserAdmin(UserAdmin):
    inlines = ()
    def get_inline_instances(self, request, obj=None):
        try:
            user_p = Profile.objects.get(user_id=obj.id)
            #forma_pago = FormaPago.objects.get(user_id=obj.id)
            self.inlines = (ProfileInline,FormaPagoInline)
        except:
        	self.inlines = ()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)