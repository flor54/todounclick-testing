from django.urls import path,re_path,include
from .views import *
name_app='usuarios'
urlpatterns = [
	path('', include('django.contrib.auth.urls')),
	path('signin/', userRegistration, name='signin'),
    path('signin/confirm-email/<int:user_id>', activate,name='confirm_email'),
	#re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',activate, name='activate'),
    path('signin/profile-info/<int:user_id>', profileData,name='profile'),
    path('signin/info-pago/<int:user_id>', infoPago,name='info-pago'),
    # path('<str:slug>/', blog_post_detail_view),
    # path('<str:slug>/edit/', blog_post_update_view),
    # path('<str:slug>/delete/', blog_post_delete_view),
]