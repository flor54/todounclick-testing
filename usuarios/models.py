from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_init,post_save,pre_save
from django.dispatch import receiver


# Create your models here.

from django.contrib.auth.models import User

#auto_now_add=True
class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE,null=True)
	f_nacimiento   = models.DateField(null=True, blank=True)
	dni		= models.CharField(max_length=10)
	baja	= models.BooleanField(default=False,blank=True)
	domicilio = models.CharField(max_length=255,default='')
	# campos opcionales
	usuario_referencia   = models.CharField(max_length=100,null=True,blank=True)
	def __str__(self):
		return self.user.username


class FormaPago(models.Model):
	user 	= models.OneToOneField(User, on_delete=models.CASCADE,null=True)
	#datos cuenta bancaria
	cta_bancaria 	= models.CharField(max_length=16)

	# datos credito
	nro_tarjeta_credito	= models.CharField(max_length=16)
	nombre_apellido = models.CharField(max_length=150)
	vencimiento = models.CharField(max_length=5)
	digitos = models.CharField(max_length=3)
	def __str__(self):
		return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance,created, **kwargs):
	if created and not instance.is_staff and not instance.is_superuser:
		profile = Profile.objects.create(user=instance)
		forma_pago = FormaPago.objects.create(user=instance)
		profile.save()
		forma_pago.save()



#este codigo esta para revisar 
@receiver(post_save, sender=Profile)
def create_or_update_profile(sender, instance, created, **kwargs):
	if not created and instance.usuario_referencia:
		try:
			usur_ref = User.objects.get(username=instance.usuario_referencia)
		except:
			raise ValidationError('usuario referencia no exixte')