from django.test import TestCase
from django.contrib.auth.models import User
from .models import Profile


class ProfileTest(TestCase):
	'''
	test bordes sobre valores input para la db no se prueban porque
	el framework ya cubre esos casos.
	'''

	def test_admin_is_not_client(self):
		'''
		 si el usuario es superuser (Admin)
		 no puede ser Profile/Cliente.
		 este test verfica que cuando 
		 se cree un superusuario no deba crearse un
		 perfil cliente automaticamente.
		'''
		adminUser = User.objects.create_superuser('admin','admin@gmail.com','zaraza')
		user_p = Profile.objects.filter(user_id=adminUser.id).exists()
		self.assertFalse(user_p)

	def test_create_profile(self):
		'''
		en caso de crear un usario que no es superuser, 
		entonces el usario es cliente y tiene un perfil
		'''
		user = User.objects.create_user('user','user@gmail.com','user123')
		user_p = Profile.objects.filter(user_id=user.id).exists()
		self.assertTrue(user_p)
