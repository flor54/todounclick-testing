from django import forms
from .models import Profile, FormaPago
from django.contrib.auth.models import User
import datetime

class UserRegistrationForm(forms.ModelForm):
	password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'itemForm'}))
	confirm_password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'itemForm'}))
	email = forms.EmailField(widget=forms.TextInput(attrs={'class':'itemForm'}))
	username = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm'}))
	first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm'}))
	last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm'}))
	class Meta:
		model = User
		fields = [
			'email',
			'username',
			'first_name',
			'last_name',
			'password',
			]

	def clean_confirm_password(self):
		cleaned_data = super(UserRegistrationForm, self).clean()
		password = cleaned_data.get("password")
		confirm_password = cleaned_data.get("confirm_password")
		if password != confirm_password:
			raise forms.ValidationError(
			    "password and confirm_password does not match"
			)
	def clean_email(self):
		email = self.cleaned_data.get("email")
		qs = User.objects.filter(email__iexact=email)
		if qs.exists():
			raise forms.ValidationError("No puedes usar este email. Ya esta registrado.")
		return email

	def save(self, commit=True):
		# Save the provided password in hashed format
		user = super(UserRegistrationForm, self).save(commit=False)
		user.set_password(self.cleaned_data["password"])
		if commit:
			user.save()
		return user
		
YEARS= [x for x in range(1940,2021)]

class ProfileForm(forms.ModelForm):
	f_nacimiento = forms.DateField(widget=forms.DateInput(attrs={'class':'itemForm','type':'date'}))
	dni = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm'}))
	domicilio = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm'}))
	usuario_referencia = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'itemForm'}))

	class Meta:
		model = Profile
		fields = [
			'f_nacimiento',
			'dni',
			'domicilio',
			'usuario_referencia',
			]
	def clean_usuario_referencia(self):
		usuario_referencia = self.cleaned_data.get("usuario_referencia")
		if not len(usuario_referencia) == 0:			
			qs = User.objects.filter(username=usuario_referencia)
			if not qs.exists():
				raise forms.ValidationError("Usuariode referencia no existe.")
		else:
			return usuario_referencia

	def clean_f_nacimiento(self):
		f_nacimiento = self.cleaned_data.get('f_nacimiento')
		f_actual = datetime.date.today()
		año_actual = f_actual.year
		mayor_edad = 21
		f_nacimiento_mod = datetime.date(year=año_actual,month=f_nacimiento.month,day=f_nacimiento.day)
		if año_actual - f_nacimiento.year >= mayor_edad:
			if año_actual - f_nacimiento.year == mayor_edad:
				if (f_nacimiento.month <= f_actual.month and f_nacimiento.day <= f_actual.day):
					return f_nacimiento
			if (año_actual - f_nacimiento.year > mayor_edad):
				return f_nacimiento
		
		raise forms.ValidationError('No eres mayor de edad.')
		# año actual:2019 f_nac_mod  2019-11-13   f_actual 2019-11-17
		#raise forms.ValidationError(f_nacimiento.format())



class InfoPago(forms.ModelForm):
	nro_tarjeta_credito = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm'}))
	nombre_apellido = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm'}))
	vencimiento = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm','value':'00/00'}))
	digitos = forms.CharField(widget=forms.TextInput(attrs={'class':'itemForm'}))	

	class Meta:
		model = FormaPago
		fields = [
			'nro_tarjeta_credito',
			'nombre_apellido',
			'vencimiento',
			'digitos',
			]
	def clean_vencimiento(self):
		vencimiento = self.cleaned_data.get('vencimiento')
		try:
			datetime.datetime.strptime('01/'+str(vencimiento),"%m/%d/%y")
			return vencimiento
		except ValueError as err:
			raise forms.ValidationError('Ingresa una fecha.')
