from django.shortcuts import render,redirect,get_object_or_404
from django.views.generic.edit import FormView
from django.contrib import messages
from .forms import *
# from django.http import HttpResponse
# from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
# from django.utils.encoding import force_bytes, force_text
# from .tokens import account_activation_token
# from django.core.mail import EmailMessage
# from django.contrib.auth import login, authenticate
# from django.contrib.sites.shortcuts import get_current_site
# from django.template.loader import render_to_string


# Create your views here.

class UserRegistrarionView(FormView):
    template_name = 'register.html'
    form_class = UserRegistrationForm
    success_message = 'Cuenta creada. Te enviamos un correo electronico para que confirmes tu email'
    success_url = 'confirm-email/'

    def form_valid(self, form):
        return super().form_valid(form)

def userRegistration(request):
	form = UserRegistrationForm(request.POST or None)
	if request.method == 'POST':
		if form.is_valid():
			messages.success(request, 'Te enviamos un mail de confirmacion')
			user = form.save()
			user.is_activate = False
			user.save()
			# current_site = get_current_site(request)
			# mail_subject = 'Activate your blog account.'
			# message = render_to_string('acc_active_email.html', {
			# 	'user': user,
			# 	'domain': current_site.domain,
			# 	'uid':urlsafe_base64_encode(force_bytes(user.pk)),
			# 	'token':account_activation_token.make_token(user),
			# 	})
			# to_email = form.cleaned_data.get('email')
			# email = EmailMessage(
			# mail_subject, message, to=[to_email]
			# )
			# email.send()
			#return HttpResponse('Please confirm your email address to complete the registration')
			#form = UserRegistrationForm()
			return redirect('confirm_email',user_id=user.id)
	context =  {'form': form }
	return render(request, 'register.html',context)


# def confirm_email(request,username=None):
# 	 template_name = 'confirm-email.html'
# 	 context = {}
# 	 return render(request,template_name,context)

def activate(request,user_id=None):
    # try:
    #     uid = force_text(urlsafe_base64_decode(uidb64))
    #     user = User.objects.get(pk=uid)
    # except(TypeError, ValueError, OverflowError, User.DoesNotExist):
    #     user = None
    # if user is not None and account_activation_token.check_token(user, token):
    #     user.is_active = True
    #     user.save()
    #     login(request, user)
    #     # return redirect('home')
    #     return HttpResponse('Gracias por confirmar tu email!.')
    # else:
    #     return HttpResponse('Activation link is invalid!')
	template_name = 'confirm-email.html'
	context = {'user_id':user_id}
	return render(request,template_name,context)

def profileData(request,user_id=None):
	template_name = 'register-2.html'
	instance = get_object_or_404(Profile, user_id=user_id)
	user = User.objects.get(id=user_id)
	form = ProfileForm(request.POST or None, instance=instance)
	if form.is_valid():
		form.save()
		return redirect('info-pago',user_id=user_id)
	context = {
		'username':user.username,
		'user_id' : user_id,
		'form':form,
	}
	return render(request,template_name,context)

def infoPago(request,user_id=None):
	template_name = 'register-3.html'
	user = User.objects.get(id=user_id)
	instance = get_object_or_404(FormaPago, user_id=user_id)
	form = InfoPago(request.POST or None, instance=instance)
	if form.is_valid():
		form.save()
		return redirect('lista_productos')
	context = {
		'username':user.username,
		'user_id' : user_id,
		'form':form,
	}
	return render(request,template_name,context)

def login(self):
	template_name = 'login.html'
	context = {}
	
	return render(request,template_name,context)