from django.db import models
from django.urls import reverse
from django.db.models.signals import pre_save
# Create your models here.


PAIS = (
    ('AR', 'ARGENTINA'),
    ('BR', 'BRASIL'),
    ('MX', 'MEXICO'),
    ('CH', 'CHILE'),
    ('BO', 'BOLIVIA'),
    ('VE', 'VENEZUELA'),
    #etc......
)

class Producto(models.Model):
    titulo = models.CharField(max_length= 120,null=False)
    descripcion = models.TextField(max_length= 500,null=True)
    ubicaciion = models.CharField(max_length=2, choices=PAIS,default='AR')
    precio = models.DecimalField(max_digits=10,decimal_places=2,null=False)
    slug = models.SlugField(max_length=200,blank= True,unique= True)

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return reverse("producto:descripcion", kwargs={"slug": self.slug})

def create_slug(instance, new_slug=None):
    slug = slugify(instance.titulo)
    if new_slug is not None:
        slug = new_slug
    qs = Producto.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug

from .utils import unique_slug_generator

def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        # instance.slug = create_slug(instance)
        instance.slug = unique_slug_generator(instance)


    

pre_save.connect(pre_save_post_receiver, sender=Producto)