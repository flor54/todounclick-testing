from django.test import TestCase
from .models import Producto
# Create your tests here.

class ModeloProductoTest(TestCase):
	def setUp(self):
		producto = Producto(titulo='12 producto',
			descripcion='descripcion del producto...',
			precio=1277.99)
		
	def test_slug_creator(self):
		# se prueba que el slug autogerado funcione con el remplazo de espacios por -
		prueba_producto = Producto.objects.get(titulo='12 producto')
		self.assertEquals(prueba_producto.slug, '12-producto')
	
	def tearDown(self):
		pass