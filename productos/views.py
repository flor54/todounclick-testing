from django.shortcuts import render
from .models import *
# Create your views here.


def lista_productos(request):
	qs1  = Producto.objects.filter(descripcion__icontains='Automovil')
	qs2  = Producto.objects.filter(descripcion__icontains='Hotel')
	template_name = 'lista_productos.html'
	context = {
		'productos1' : qs1,
		'productos2': qs2,
	}
	return render(request,template_name,context)