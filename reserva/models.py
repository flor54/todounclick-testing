from django.db import models
from productos.models import Producto
from usuarios.models import User
# Create your models here.
class Reserva(models.Model):
    productos = models.ManyToManyField(Producto)
    user = models.ForeignKey(User,on_delete=models.CASCADE,null=True)
    totalDolar = models.DecimalField(max_digits=10,decimal_places=2,null=False)
    totalPeso = models.DecimalField(max_digits=10,decimal_places=2,null=False)